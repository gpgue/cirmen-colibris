package fr.gouv.edu.dagsi.cirmen.colibris.bshe.model;

public class ColibrisBsheConge {
	private String congeAnnee;
	private String congeDureeMois;
	private String congeDureeJours;
	private String congeLieu;
	public String getCongeAnnee() {
		return congeAnnee;
	}
	public void setCongeAnnee(String congeAnnee) {
		this.congeAnnee = congeAnnee;
	}
	public String getCongeDureeMois() {
		return congeDureeMois;
	}
	public void setCongeDureeMois(String congeDureeMois) {
		this.congeDureeMois = congeDureeMois;
	}
	public String getCongeDureeJours() {
		return congeDureeJours;
	}
	public void setCongeDureeJours(String congeDureeJours) {
		this.congeDureeJours = congeDureeJours;
	}
	public String getCongeLieu() {
		return congeLieu;
	}
	public void setCongeLieu(String congeLieu) {
		this.congeLieu = congeLieu;
	}
}
