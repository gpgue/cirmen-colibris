package fr.gouv.edu.dagsi.cirmen.colibris.bshe.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import fr.edu.lyon.publik.wcs.model.Form;

public class ColibrisBsheForm extends Form {
	
	@JsonProperty("fields")
	private BsheDemande demande;

	public BsheDemande getDemande() {
		return demande;
	}

	public void setDemande(BsheDemande demande) {
		this.demande = demande;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public class BsheDemande {
		private String actionAttendue;
		private String nom;
		private String prenoms;
		private String nir;
		private String grade;
		@JsonProperty("conges_raw")
		private List<ColibrisBsheConge> conges;
		
		private List<ColibrisBsheAffectation> affectations;
		
		// attributs sans getters, utilitaires reflet du formulaire colibris
		private Integer nb_aff;
		private String territoire_1;
		private String debut_aff_1;
		private String fin_aff_1;
		private String territoire_2;
		private String debut_aff_2;
		private String fin_aff_2;

		public void setDebut_aff_1(String debut_aff_1) {
			this.debut_aff_1 = debut_aff_1;
		}

		public void setFin_aff_1(String fin_aff_1) {
			this.fin_aff_1 = fin_aff_1;
		}

		public void setTerritoire_1(String territoire_1) {
			this.territoire_1 = territoire_1;
		}

		public void setNb_aff(Integer nb_aff) {
			this.nb_aff = nb_aff;
		}

		// attributs accessibles ( avec getter ) 

		public List<ColibrisBsheAffectation> getAffectations() {
			if (affectations == null) {
				affectations = new ArrayList<ColibrisBsheAffectation>();
				affectations.add(new ColibrisBsheAffectation(territoire_1,debut_aff_1,fin_aff_1));
				affectations.add(new ColibrisBsheAffectation(territoire_2,debut_aff_2,fin_aff_2));
			}
			return affectations;
		}

		public List<ColibrisBsheConge> getConges() {
			return conges;
		}

		public void setConges(List<ColibrisBsheConge> conges) {
			this.conges = conges;
		}

		public String getPrenoms() {
			return prenoms;
		}

		public void setPrenoms(String prenoms) {
			this.prenoms = prenoms;
		}

		public String getNir() {
			return nir;
		}

		public void setNir(String nir) {
			this.nir = nir;
		}

		public String getGrade() {
			return grade;
		}

		public void setGrade(String grade) {
			this.grade = grade;
		}

		public String getActionAttendue() {
			return actionAttendue;
		}

		public void setActionAttendue(String actionAttendue) {
			this.actionAttendue = actionAttendue;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		} 
		
	}

}
