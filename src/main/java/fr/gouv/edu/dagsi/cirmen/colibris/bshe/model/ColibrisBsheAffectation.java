package fr.gouv.edu.dagsi.cirmen.colibris.bshe.model;

public class ColibrisBsheAffectation {

	private String territoire;
	private String dateDebut;
	private String dateFin;
	
	public ColibrisBsheAffectation(String territoire, String date_debut, String date_fin) {
		this.territoire = territoire;
		this.dateDebut = date_debut;
		this.dateFin = date_fin;
	}

	public String getTerritoire() {
		return territoire;
	}

	public void setTerritoire(String territoire) {
		this.territoire = territoire;
	}

	public String getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

}
