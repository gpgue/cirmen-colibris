package fr.gouv.edu.dagsi.cirmen.colibris.bshe;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import fr.edu.lyon.publik.PublikApiException;
import fr.edu.lyon.publik.authenticator.WcsAuthenticator;
import fr.edu.lyon.publik.client.Client;
import fr.edu.lyon.publik.wcs.service.WcsApiService;
import fr.gouv.edu.dagsi.cirmen.colibris.bshe.model.ColibrisBsheForm;

public class ColibrisBsheService {

	// propriétés
	static private String bundleFilename = ColibrisBsheService.class.getName();
	
	// connexion Colibris
	static private String KEY_key = "ColibrisBsheService.key";
	static private String KEY_origine = "ColibrisBsheService.origine";
	static private String KEY_url = "ColibrisBsheService.url";
	static private String KEY_user = "ColibrisBsheService.user";
	static private String KEY_charset = "ColibrisBsheService.charset";
	static private String KEY_statut = "ColibrisBsheService.statut";

	// formulaire BSHE
	static private String KEY_user_carmen = "ColibrisBsheService.user_carmen";
	static private String KEY_slug="ColibrisBsheService.slug";
	static private String KEY_statut_nouveau="ColibrisBsheService.statut_nouveau";
	static private String KEY_statut_refuse="ColibrisBsheService.statut_refuse";
	static private String KEY_statut_accepte="ColibrisBsheService.statut_accepte";
	static private String KEY_trigger_refuse="ColibrisBsheService.trigger_refuse";
	static private String KEY_trigger_accepte="ColibrisBsheService.trigger_accepte";
	static private String KEY_trigger_nouveau="ColibrisBsheService.trigger_nouveau";
	static private String KEY_amend="ColibrisBsheService.amend";

	// méthode utilitaire de récupération des propriétés
	static private String r(String propertyName) {
		return ResourceBundle.getBundle(bundleFilename).getString(propertyName);
	}
	
	// client  
	static private Client client;

	// service
	static private WcsApiService service;

	///////////// méthodes publiques 

	/**
	 * Lister les nouvelles demandes BSHE
	 * @return
	 * @throws PublikApiException
	 */
	public static List<Integer> getNouvellesDemandes() throws PublikApiException {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(r(KEY_statut), r(KEY_statut_nouveau));
		List<Integer> formIds = getService().getFormIds(r(KEY_slug), filters);
		return formIds;
	}

	/**
	 * Lire une demande BSHE
	 * @param demande
	 * @return
	 * @throws PublikApiException
	 */
	public static ColibrisBsheForm lire(int demande) throws PublikApiException {
		return getService().getForm(r(KEY_slug), demande, ColibrisBsheForm.class);
	}
	
	/**
	 * Refuser une demande BSHE ( changement de statut ) 
	 * @param demande
	 * @throws PublikApiException
	 */
	public static void refuser(int demande) throws PublikApiException {
		getService().followFormTransition(r(KEY_slug), demande, r(KEY_trigger_refuse), null);
	}
	
	/**
	 * Accepter une demande BSHE ( changement de statut ) 
	 * @param demande
	 * @throws PublikApiException
	 */
	public static void accepter(int demande) throws PublikApiException {
		getService().followFormTransition(r(KEY_slug), demande, r(KEY_trigger_accepte), null);
	}
	
	/**
	 * Renseigner la zone prevue pour expliquer un refus
	 * @param demande
	 * @param raison
	 * @throws PublikApiException
	 */
	public static void amender(int demande, String raison) throws PublikApiException {
		String data = r(KEY_amend);
		data = data.format(data, raison, r(KEY_user_carmen));
		service.updateForm(r(KEY_slug), demande, data.getBytes(Charset.forName(r(KEY_charset))));
	}
	
	///////////// méthodes privées
	
	// méthode privée car le workflow (sauf en dev) ne permet pas de repasser à "nouveau"
	static private void nouveau(int demande) throws PublikApiException {
		getService().followFormTransition(r(KEY_slug), demande, r(KEY_trigger_nouveau), null);
	}

	static private Client getClient() {
		if (client == null) {
			client = new Client(
					r(KEY_url),
					new WcsAuthenticator().setOrigin(r(KEY_origine)).setSecretKey(r(KEY_key)).setUser(r(KEY_user))
				);
		}
		return client;
	}
	static private WcsApiService getService() {
		if (service == null) { service = new WcsApiService(getClient()); }
		return service;
	}
	
	static public String creer(byte[] content) throws PublikApiException {
		return getService().createForm(r(KEY_slug), content).getData().getId();
	}

}
