package fr.gouv.edu.dagsi.cirmen.bshe.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public interface FileMocker
{
	default byte[] mockResult(String filename)
	{
		try(InputStream in=getClass().getResourceAsStream(filename))
		{
			return IOUtils.readFully(in, in.available());
		}catch(IOException e)
		{
			return "".getBytes();
		}
	}
}
