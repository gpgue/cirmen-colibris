package fr.gouv.edu.dagsi.cirmen.bshe;

import fr.gouv.edu.dagsi.cirmen.colibris.bshe.ColibrisBsheService;
import fr.gouv.edu.dagsi.cirmen.colibris.bshe.model.ColibrisBsheForm;

public class ColibrisBsheEssai
{
	/**
	 * A utiliser en dev 
	 */
    public static void main (String[] args) 
    {
    	try {
	    	int d = 72;
	    	
	    	// lire une demande en particulier, celle du test
			ColibrisBsheForm f = ColibrisBsheService.lire(d);
			
			// passer la demande en statut 'anomalie' (refusé)
			// ColibrisBsheService.refuser(d);
			
			// indiquer (ajouter) les raisons de ce refus (les anomalies)
			ColibrisBsheService.amender(d,"Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
					+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max ");

    	} catch (Throwable t) {
			t.printStackTrace();
		}
    }

}
