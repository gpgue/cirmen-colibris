package fr.gouv.edu.dagsi.cirmen.bshe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.edu.lyon.publik.PublikApiException;
import fr.edu.lyon.publik.wcs.model.Form;
import fr.gouv.edu.dagsi.cirmen.bshe.utils.FileMocker;
import fr.gouv.edu.dagsi.cirmen.colibris.bshe.ColibrisBsheService;
import fr.gouv.edu.dagsi.cirmen.colibris.bshe.model.ColibrisBsheAffectation;
import fr.gouv.edu.dagsi.cirmen.colibris.bshe.model.ColibrisBsheConge;
import fr.gouv.edu.dagsi.cirmen.colibris.bshe.model.ColibrisBsheForm;

/**
 * Unit test
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ColibrisBsheTest implements FileMocker
{
	
	// Fichier resource utilisé pour créer une nouvelle demande
	private static String testFileA = "/fr/gouv/edu/dagsi/cirmen/colibris/bshe/bsheTestCreer.json";
	private static String testFileB = "/fr/gouv/edu/dagsi/cirmen/colibris/bshe/bsheTestCreerAvecConges.json"; // @TODO : problème pour créer avec bloc de champs 

    /**
     * Lire les nouvelles demandes
     * @throws PublikApiException
     */
	@Test
    public void testB_lireLesNouvellesDemandes() throws PublikApiException
	{
    	// créer une nouvelle demande BSHE
		Integer d = creerNouvelleDemandeBshe();

		// lire les demandes en statut 'nouveau' (reçue).
    	List<Integer> formIds = ColibrisBsheService.getNouvellesDemandes();
    	assertTrue(formIds.size()>0);
	}

	/**
     * Cas : Lire une nouvelle demande,
     * Lire les congés
     * Lire les affectations
     * Mettre en statut anomalie
     * Renseigner la raison
     * 
     * @throws PublikApiException 
     */
    @Test
    public void testC_mettreUneDemandeBsheEnAnomalie() throws PublikApiException
    {
    	// créer une nouvelle demande BSHE
		Integer d = creerNouvelleDemandeBshe();

    	// lire une demande en particulier, celle du test
		ColibrisBsheForm f = ColibrisBsheService.lire(d);
		
		// Vérification congé
		List<ColibrisBsheConge> conges = f.getDemande().getConges();
		// assertEquals(conges.get(0).getCongeLieu(),"Saint-Maclou"); // @TODO : problème pour créer avec bloc de champs

		// Vérification affectation
		List<ColibrisBsheAffectation> affs = f.getDemande().getAffectations();
		assertEquals(affs.get(0).getDateDebut(),"2022-02-06");

		// passer la demande en statut 'anomalie' (refusé)
		ColibrisBsheService.refuser(d);
		
		// indiquer (ajouter) les raisons de ce refus (les anomalies)
		ColibrisBsheService.amender(d,"Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max "
				+ "Parce que test longueur max Parce que test longueur max Parce que test longueur max ");
		
    }
    
    /**
     * Cas : Mettre une nouvelle demande en statut validée.
     * 
     * @throws PublikApiException 
     */
    @Test
    public void testD_mettreUneDemandeBsheEnValide() throws PublikApiException
    {
    	// créer une nouvelle demande BSHE
		Integer d = creerNouvelleDemandeBshe();

    	// mettre cette demande à l'état acceptée (validée).
		ColibrisBsheService.accepter(d);
    }

	// créer une nouvelle demande BSHE
    private Integer creerNouvelleDemandeBshe() throws PublikApiException {
    	String s = ColibrisBsheService.creer(mockResult(testFileA));
		return new Integer(s);
    }

}
