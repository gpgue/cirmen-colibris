- [Interface entre Colibris et l'application Cirmen-Carmen.](#interface-entre-colibris-et-lapplication-cirmen-carmen)
	- [Utilisation](#utilisation)
	- [Formulaires BSHE](#formulaires-bshe)
		- [Utilisation](#utilisation-1)
		- [Paramétrage](#paramétrage)
		- [Resource](#resource)
	- [(*) Problème d'accès réseau des serveurs SREN](#-problème-daccès-réseau-des-serveurs-sren)
  
# Interface entre Colibris et l'application Cirmen-Carmen.

Interface entre Colibris et l'application Cirmen-Carmen.

Colibris est le système de gestion de formulaires destinés aux agents du ministère qui utilise l'application Publik (open source, https://doc-publik.entrouvert.com).

Cette interface est utilisée par l'application Carmen afin d'intégrer les demandes de Bonifications Service Hors Europe (BSHE) effectuées par les agents, pour leurs retraites, sur Colibris.

Cette interface utilise le code 'client publik' développé par l'académie de Lyon : la dépendance est déclarée dans la configuration `maven`.

## Utilisation

- Déclarer la dépendance maven (dans Carmen).
```xml
	<groupId>fr.gouv.edu.dagsi.cirmen</groupId>
	<artifactId>cirmen-colibris</artifactId>
	<version>27.100.00-SNAPSHOT</version>
```
(*) voir en fin de doc pour l'accès depuis le SREN

## Formulaires BSHE
### Utilisation

Les méthodes proposées par l'API permettent de réaliser le fonctionnement décrit sur ce document confluence : https://sirhmen.atlassian.net/l/c/a4SAdFeb

- Lire les nouvelles demandes
```java
List<Integer> formIds = ColibrisBsheService.getNouvellesDemandes();
```

- Lire une demande en particulier
```java
ColibrisBsheForm f = ColibrisBsheService.lire(formId);
```

- Lire les données d'une demande (NIR, nom, prenom, grade, date de demande, statut, congés, affectations ...).
```java
//congés
List<ColibrisBsheConge> conges = f.getDemande().getConges();

// affecations
List<ColibrisBsheAffectation> affs = f.getDemande().getAffectations();

//date de début de la première affectation
affs.get(0).getDateDebut(); 
...
```

- Passer la demande en statut 'anomalie' (refusé)
```java
ColibrisBsheService.refuser(d);
```

- Indiquer les raisons de ce refus (les anomalies)
```java
ColibrisBsheService.amender(d,"Sortie des règles de gestion Carmen ... ");
```

- Mettre cette demande à l'état acceptée (validée).
```java
ColibrisBsheService.accepter(d);
```

### Paramétrage

Le paramétrage de l'interface s'effectue via un fichier resource
à placer dans le chemin des classes Java et nommé :

`fr/gouv/edu/dagsi/cirmen/colibris/bshe/ColibrisBsheService.properties`

- Dans ce fichier, les paramètres d'**infrastructure** sont : 

```
ColibrisBsheService.url=https://formulaires-demo.valere.ac-lyon.fr/api
ColibrisBsheService.key=fc4bc6f7-1a20-4d87-9f0e-24bfa930bae9
ColibrisBsheService.origine=carmen_api
```

Seuls ces paramètres ci-dessus sont à modifier lors d'une **installation** en fonction de l'**environnement** (production, test ...).

- Ceux liés au formulaire sont :

```
ColibrisBsheService.charset=UTF8
ColibrisBsheService.slug=bshe-frantz
ColibrisBsheService.statut=statut
ColibrisBsheService.user=carmen.labaule@education.gouv.fr
ColibrisBsheService.user_carmen=carmen.labaule@education.gouv.fr
ColibrisBsheService.statut_nouveau=1
ColibrisBsheService.statut_accepte=2
ColibrisBsheService.statut_refuse=3
ColibrisBsheService.trigger_refuse=refuser
ColibrisBsheService.trigger_accepte=accepter
ColibrisBsheService.trigger_nouveau=nouveau
ColibrisBsheService.amend={ \"data\": { \"actionAttendue\": \"%s\" }, \"user\": { \"email\": \"%s\" } }
```

Ils sont à modifier en fonction du formulaire.

### Resource

Un export du formulaire et du workflow est stocké dans le répertoire resource du projet.

A ce jour il s'agit de formulaires développés sur une plate-forme Colibris de démonstration, destinés à être installés via importation sur les serveurs ad-hoc lorsqu'ils seront disponibles.

## (*) Problème d'accès réseau des serveurs SREN
Le serveur `Nexus` du SREN n'est pas à jour car les accès réseau ne lui permettent pas d'accéder au dépot central maven.
Cela empèche de récupérer les dépendances commons-i0 et maven-plugin nécessaire.
En passant par le serveur Nexus de la Forge, c'est possible.
Pour ce faire, ajouter un 'mirror', c-a-d les lignes ci-après, dans le fichier '~/.m2/settings.xml' :
```xml
...
	<mirrors>
		<mirror>
			<id>nexus-forge</id>
			<name>Nexus proxy</name>
			<url>https://repository-maven.forge.education.gouv.fr/public/</url>
			<mirrorOf>*</mirrorOf>
		</mirror>
...
```
> Si le projet est récupéré par l'utilisateur maven connecté sur umatraii ( le Nexus SREN ), et une commande maven lancée sur le projet, alors les bibliothèques seront récupérées, dans répertoire `.m2` de l'utilisateur `maven`.
> Ensuite le serveur `Nexus` du SREN trouvera bien les dépendances.
